
public interface Vector {
	public void add(Vector v);
    public void numberMultiply(double a);
    public double scalarProduct(Vector v);
}
